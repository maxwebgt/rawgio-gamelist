import React from 'react';
import {Link} from "react-router-dom";

import styled from "styled-components";
const Ul = styled.ul`
  font-size: 18px;
  padding: 10px;
  margin: 10px;
  display: flex;
  justify-content: space-around;
  //background: papayawhip;
  li {  
    list-style-type: none
  }
`;


const Menu = () => {
    return (
        <nav>
            <Ul>
                <li>
                    <Link to="/">HOME</Link>
                </li>
            </Ul>
        </nav>
    );
};

export default Menu;