import React from 'react';
import { Link, useParams } from "react-router-dom";
import styled from "styled-components";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';
import Loader from "react-loader-spinner";

const ItemFull = () => {
    let { id } = useParams();
    let [item, setItem] = React.useState(null)
    let [screenshots, setScreenshots] = React.useState(null)

    React.useEffect(() => {
        fetch(`https://api.rawg.io/api/games/${id}`).then(response => response.json()).then(data => setItem(data))
        fetch(`https://api.rawg.io/api/games/${id}/screenshots`).then(response => response.json()).then(data => setScreenshots(data.results))
    }, []);

    let [slide, setSlide] = React.useState(0)

    const next = () => {
        setSlide(slide + 1)
    };

    const prev = () => {
        setSlide(slide - 1)
    };

    const updateCurrentSlide = (index) => {
        if (slide !== index) { setSlide(index) }
    };

    return (
            !item ? <Loader type="TailSpin" color="#000000" height={80} width={80} /> :
                <CardWrapper>
                    <Nav><Link to="/">Catalog</Link><span>&nbsp;->&nbsp;</span><b>{item.name}</b></Nav>

                    <Hr/>

                    <Card>
                        <CardImg src={item.background_image} alt=""/>
                        <CardTitle>{item.name}</CardTitle>
                        <CardLink href={item.website}>Go to website</CardLink>
                    </Card>

                    <p>{item.description_raw}</p>

                    {/*<p>External slide value: {slide}</p>*/}
                    <SliderControls>
                        <SliderButton onClick={()=>{prev()}} >
                            <IconArrowLeft/>
                        </SliderButton>
                        <SliderButton onClick={()=>{next()}} >
                            <IconArrowRight/>
                        </SliderButton>
                    </SliderControls>
                    {
                        !screenshots ? <Loader type="TailSpin" color="#000000" height={80} width={80} /> :
                        <Carousel
                            autoPlay
                            selectedItem={slide}
                            onChange={updateCurrentSlide}
                            renderArrowPrev = {()=>{}}
                            renderArrowNext = {()=>{}}
                        >
                            {
                                screenshots.map(e => <div key={e.id}><img src={e.image} /></div>)
                            }
                        </Carousel>
                    }
                </CardWrapper>
    );
};

export default ItemFull;

// ******* Styles *******

const Card = styled.div`
  position: relative;
`

const SliderControls = styled.div`
  display: flex;
  justify-content: space-around;
  width: 150px;
  margin: 15px auto;
`

const CardTitle = styled.div`
  text-align: center;
  padding: 15px;
  background-color: rgba(0,0,0,.4);
  color: white;
  font-size: 4vh;
  position: absolute;
  font-weight: 800;
  left: 0;
  right: 0;
  top: 0;
`

const CardImg = styled.img`
  width: 100%;
`
const CardWrapper = styled.div`  
`
const Nav = styled.nav`
  margin: 10px 0 5px 0;
  a {
    font-size: 25px;
    color: black;    
    text-decoration: none;
    font-weight: 700;    
  }
`

const Hr = styled.hr`
  margin-bottom: 15px;
`

const SliderButton = styled.button`
  background-color: rgba(0,0,0,.3);
  border: none;
  padding: 11.5px 13px; 
  border-radius: 50%;
  outline: none;
  cursor: pointer;
  :hover {
    background-color: rgba(0,0,0);
  }
`

const CardLink = styled.a`
  position: absolute;
  font-size: 3vh;
  left: 0;
  right: 0;
  bottom: 0;
  padding: 10px;
  background-color: rgba(0,0,0,.3);
  color: white;
  text-align: center;
  :hover {
    text-decoration: none;
  }
`

// ******* Icons *******

function IconArrowLeft(props) {
    return (
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" height="20px" width="20px"
             viewBox="0 0 492 492">
            <path fill="#ffffff" d="M198.608,246.104L382.664,62.04c5.068-5.056,7.856-11.816,7.856-19.024c0-7.212-2.788-13.968-7.856-19.032l-16.128-16.12
			C361.476,2.792,354.712,0,347.504,0s-13.964,2.792-19.028,7.864L109.328,227.008c-5.084,5.08-7.868,11.868-7.848,19.084
			c-0.02,7.248,2.76,14.028,7.848,19.112l218.944,218.932c5.064,5.072,11.82,7.864,19.032,7.864c7.208,0,13.964-2.792,19.032-7.864
			l16.124-16.12c10.492-10.492,10.492-27.572,0-38.06L198.608,246.104z"/>
        </svg>
    )
}

function IconArrowRight(props) {
    return (
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" height="20px" width="20px"
             viewBox="0 0 492 492">
            <path fill="#ffffff" d="M382.678,226.804L163.73,7.86C158.666,2.792,151.906,0,144.698,0s-13.968,2.792-19.032,7.86l-16.124,16.12
			c-10.492,10.504-10.492,27.576,0,38.064L293.398,245.9l-184.06,184.06c-5.064,5.068-7.86,11.824-7.86,19.028
			c0,7.212,2.796,13.968,7.86,19.04l16.124,16.116c5.068,5.068,11.824,7.86,19.032,7.86s13.968-2.792,19.032-7.86L382.678,265
			c5.076-5.084,7.864-11.872,7.848-19.088C390.542,238.668,387.754,231.884,382.678,226.804z"/>
        </svg>
    )
}




