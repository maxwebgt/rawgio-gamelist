import React from 'react';
import { Link } from "react-router-dom";
import styled from "styled-components";

const Item = prop => {
    const { name, released, background_image, rating, id } = prop.item
    return (
        <GameCard>
            <GameCardImg src={background_image}/>
            <GameCardRating>{rating}</GameCardRating>
            <GameCardDesc>
                <h5>{name}</h5>
                <hr/>
                <GameCardDescDate>
                    <div>Release date:</div><div>{released}</div>
                </GameCardDescDate>
                <Link to={`/game/${id}`}>Show more...</Link>
            </GameCardDesc>
        </GameCard>
    );
};

export default Item;

// ******* Styles *******

const GameCard = styled.div`
  font-size: 18px;  
  background: darkgray;
  border: none;
  border-radius: 3px;
  position: relative;
  h5 {
    margin: 5px 0 10px;
    font-weight: 700;    
    font-size: 1.3em;
  }
  a {    
    text-align: center;
    display: block;
    text-decoration: none;
    padding: 10px 5px;
    background: black;
    color: whitesmoke;
    font-weight: 700;
    filter: opacity(30%);
    transition: filter 0.5s;
    :hover {
      color: white;
      filter: opacity(100%);
    }
  }
`;

const GameCardImg = styled.img`
    height: 200px;
    width: 100%;
    object-fit: cover;
`;

const GameCardDesc = styled.div`
  background-color: rgba(0,0,0,.3);
  padding: 10px;
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  h5 {
    color: white;
    text-align: center;
  }
`;

const GameCardDescDate = styled.div`
  display: flex;
  justify-content: space-between;
  font-size: 12px;
  margin-bottom: 10px;
  color: white;
`;

const GameCardRating = styled.div`
  color: white;
  top: 5px;
  right: 5px;
  position: absolute;  
  background-color: rgba(0,0,0,0.3);
  padding: 3px;
  border-radius: 5px;
`;