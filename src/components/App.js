import React, {Component} from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import styled from "styled-components";

import Catalog from "./Catalog";
import ItemFull from "./ItemFull";

class App extends Component {
    render() {
        return (
            <Container>
                <Router>
                        <Switch>
                            <Route path="/game/:id" children={<ItemFull />} />
                            <Route path="/" children={<Catalog/>} />
                        </Switch>
                </Router>
            </Container>
        );
    }
}

export default App;


// ******* Styles *******

const Container = styled.div`     
    max-width: 960px;
    margin: 2px auto 41px;
    padding: 0 10px;
    font-family: -apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif;
`;