import React from 'react';
import Loader from "react-loader-spinner";
import "@kunukn/react-collapse/dist/Collapse.umd.css";
import Collapse from "@kunukn/react-collapse";
import Item from "./Item";
import useDebounce from "./useDebounce";
import styled from "styled-components";


const Catalog = () => {
    const [searchTerm, setSearchTerm] = React.useState('');
    const [isSearching, setIsSearching] = React.useState(false);
    const debouncedSearchTerm = useDebounce(searchTerm, 1000);
    let [items, setItems] = React.useState(null)
    let [searchQuery, setQuery] = React.useState('')
    let [sort, setSort] = React.useState('')
    let [platformsList, setPlatformList] = React.useState([])
    let [platform, setPlatform] = React.useState('')
    const [isOpen, setIsOpen] = React.useState(false);
    let [page, setPage] = React.useState(

        {
            // startPage: 1,
            currentPage: 1,
            // nextPage: 2,
            // prevPage: 1,
            // endPage: 100
        }
    )

    const pageChanger = (newcur, endpage) => {
        let startPage = 1
        let currentPage = newcur <= 0 ? 1 : newcur >= endpage ? endpage : newcur
        let nextPage = newcur + 1 >= endpage ? endpage : newcur + 1
        let prevPage = newcur - 1 <= 0 ? 1 : newcur - 1 == endpage ? endpage : newcur - 1
        let endPage = endpage

        setPage({startPage, currentPage, nextPage, prevPage, endPage})
    }

    React.useEffect(
        () => {
            if (debouncedSearchTerm) {
                setIsSearching(true);
                page.currentPage = 1
                setQuery(debouncedSearchTerm)
            }
        },
        [debouncedSearchTerm]
    );

    React.useEffect(() => {
        fetch(`https://api.rawg.io/api/platforms`).then(response => response.json())
            .then(data => setPlatformList(data.results));
    }, []);

    React.useEffect(() => {

        setItems(null);
        // let apiQuery = `https://api.rawg.io/api/games?search=${searchQuery}&page_size=40&page=${page.currentPage}&ordering=${sort}` + ((platform) ? `&platforms=${platform}` : '')
        let apiQuery = `https://api.rawg.io/api/games?search=${searchQuery}&page_size=40&page=${page.currentPage}&search_precise=1&search_exact=1&ordering=${sort}` + ((platform) ? `&platforms=${platform}` : '')
        fetch(apiQuery).then(response => response.json())
            .then(data => {
                setIsSearching(false);
                    setItems(data.results)
                    let end = Math.round(data.count/40)
                    if (platform && end >= 250) { end = 250}
                    if (page.currentPage == 1) {
                        pageChanger(page.currentPage, (end < 1) ? 1 : end)
                    }
                }
            );
    }, [searchQuery, sort, platform, page.currentPage]);

    return (
        <div>
            <SearchInput
                placeholder='Start writing the name of the game'
                onChange={e => setSearchTerm(e.target.value) }
            />

            <PlatformTitle onClick={() => setIsOpen(state => !state)}>Select platform:</PlatformTitle>
            <Collapse isOpen={isOpen}>
                <PlatformWrapper>
                    {
                        platformsList.length == 0 ? <Loader type="TailSpin" color="#000000" height={80} width={80} /> :
                            platformsList.map(e =>
                                <PlatformItem
                                    onClick = {()=> { page.currentPage = 1; setPlatform(e.id); setIsOpen(state => !state) }}
                                    key = {e.id}
                                    style = {{ backgroundImage: `url(${e.image_background})`}}
                                >
                                    <PlatformItemTitle>{e.name}</PlatformItemTitle>
                                </PlatformItem>
                            )
                    }
                </PlatformWrapper>
            </Collapse>

            <CatalogWrapper>
                <PaginationWrapper>
                    {
                        !page.endPage ? <Loader type="ThreeDots" color="#000000" height={24} width={24}/> :
                            <Paginator>
                                <PaginationItem onClick={() => pageChanger(1, page.endPage)}>{page.startPage} </PaginationItem>
                                <PaginationItem onClick={() => pageChanger(page.currentPage - 1, page.endPage)}>{'<<'}</PaginationItem>
                                <PaginationItem>{page.currentPage}</PaginationItem>
                                <PaginationItem onClick={() => pageChanger(page.currentPage + 1, page.endPage)}>{'>>'}</PaginationItem>
                                <PaginationItem onClick={() => pageChanger(page.endPage, page.endPage)}>{page.endPage}</PaginationItem>
                            </Paginator>
                    }
                </PaginationWrapper>
                <CatalogSort>
                    <select value={sort} onChange={e => setSort(e.target.value)}>
                        <option value="-rating">Rating desc</option>
                        <option value="rating">Rating inc</option>
                        <option value="-released">Realise Date desc</option>
                        <option value="released">Realise Date inc</option>
                    </select>
                </CatalogSort>
                {
                    !items ? <Loader type="TailSpin" color="#000000" height={80} width={80} /> : items.length != 0 ? items.map((item , i) => <Item key={item.id} item={item} />) : 'elements not found'
                }
            </CatalogWrapper>
        </div>
    );
};

export default Catalog;

// ******* Styles *******

const CatalogWrapper = styled.div`
    position: relative;
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(250px, 1fr));
    grid-gap: 30px;
    padding-top: 40px;
`;
const CatalogSort = styled.div`
    select {
      outline: none;
    }
    font-size: 15px;
    position: absolute;
    right: 0;
    top: 0;
`;

const SearchInput = styled.input`
  outline: none;
    border: 5px dashed black;
    padding: 10px;
    margin-bottom: 10px;
    box-sizing: border-box;
    width: 100%;
    font-size: 20px;
    ::placeholder {
      font-size: 20px;
      color: black;
    }
`;

const PlatformTitle = styled.div`
  cursor: pointer;
  font-size: 3vh;
  text-align: center;
  font-weight: 800;
  border: 2px solid black;  
  padding: 5px;
  margin-bottom: 10px;
  border-radius: 5px;
`;

const PlatformWrapper = styled.div`
    display: grid;
    grid-template-columns: repeat(auto-fill,minmax(125px,1fr));
    grid-gap: 15px;
    margin-bottom: 15px;    
`;

const PaginationWrapper = styled.div`
  position: absolute;
  @media (max-width: 380px) {
    position: relative;
    margin: auto;
  }
`;

const Paginator = styled.ul`     
    display: flex;
    padding: 0;
    margin: 0;
`;


const PaginationItem = styled.li`
    list-style: none;
    border: 1px solid black;    
    cursor: pointer;
    padding: 2px 7px;
`;

const PlatformItem = styled.div`
    cursor: pointer;
    height: 70px;    
    border-radius: 5px;
    text-align: center;
    background-position: center;
    background-size: cover;
    background-repeat: no-repeat;
    display: flex;
    align-items: center;
    justify-content: center;
    filter: grayscale(.75);
    transition: .3s ease-in-out;
    border: 5px solid rgba(255, 0, 0, .5);
    box-sizing: border-box;
    :hover {
      filter: none;
    }
    :hover div {
      filter: opacity(100%);      
    }
`;

const PlatformItemTitle = styled.div`
    font-size: 14px;
    filter: opacity(70%);
    font-weight: 700;
    background-color: white;
    padding: 10px;
    line-height: 13px;
    border-radius: 35px;
    transition: .3s ease-in-out;
`;